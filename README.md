**Jason Braswell's Magic Leap Platform Assignment**

Check out the **[Postman](https://www.getpostman.com/) Collection** for sample HTTP requests: ml-platform-examples.postman_collection.json

---

## Additional Requirement(s)

1. Token-based access control. When performing a GET on a particular asset (GET /appstore/assets/{assetId}), a downloadUrl will be returned as an attribute on the returned object. This URL will only be usable for 5 minutes to download to uploaded asset file content.

2. Listing assets. A GET can be performed on the assets URI without a specified id in order to perform a List operation on the resource (GET /appstore/assets). Pagination (not fully implemented) can be used to limit the result set using the 'maxResults' query parameter (GET /appstore/assets?maxResults=1).

---

## How to build

This NodeJS project was build using [NPM](https://www.npmjs.com/) and deployed via the [Serverless Framework](https://serverless.com/) for AWS.

The easiest way to install is by following along the tutorial at https://serverless.com/framework/docs/providers/aws/guide/installation/

---

## Design decisions & considerations

- **File upload**: Files currently get uploaded to a /tmp dir on the AWS Lambda running the app. Then they are transferred to an S3 bucket. The max file size is currently set to a default 2MB. 

    -- *Scaling*: File cleanup was not implemented or researched for Lambda which could become an issue, because although Lambda can scale dynamically to meet high-traffic scenarios, the physical disk space and maximum memory usage is limited. If scaling was an issue, I would consider using S3's presigned upload capabilities to bypass the overhead caused by processing large binary content. 
    
    -- *Security*: S3 has options for encrypting data on S3 at rest, although this was not implemented. Traffic to the server would always be encrypted via TLS. 
    
    -- *Other factors*: S3 was chosen because it has high availability, low latency, and low costs. Lifecycle rules can also be added to deprecate files to even cheaper storage solutions or be removed altogether after a period of time.

- **Metrics & Monitoring**: In a production application, it's important to monitor the number of errors, latency of operations, volume spikes, database throttles, etc. There are a number of utilities to help with this, including AWS' CloudWatch service.

- **Database**: DyanmoDB was chosen for this project because it was fast to setup and schemaless. For microservice applications, especially ones with high traffic requirements, DDB is a good and flexible choice. For more relational data needs, RDS would have been the better option.

- **Pagination**: Most, if not all, LIST operations should be paginated in order to properly scale.

- **HATEOAS**: For the sake of time, did not implement, but generally a good idea. For example, in the LIST operation it would be nice to return the URI for retrieving the next page.

## Known issues

1. The file upload API has a bug when uploading non text-based files for some reason. I suspect some Express middleware is the root cause of this, but didn't want to expend too much time. For testing purposes, you can try using this readme file for your upload.