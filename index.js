const serverless = require('serverless-http');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// Load global middleware
app.use(bodyParser.json({ strict: false }));

// Load Routes
require("./routes/asset-route")(app);

app.get('/', function (req, res) {
  res.send(`This is the root URL for Jason Braswell's Magic Leap code assignment.`);
});

module.exports.handler = serverless(app);