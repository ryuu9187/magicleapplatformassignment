const { formatErrorBody } = require('../utils/response-utils');

module.exports = (defaultMaxResults) => (req, res, next) => {
    let maxResults = defaultMaxResults;

    // maxResults are defined (provided by caller)
    if (req.query.maxResults !== void 0) {
        maxResults = parseInt(req.query.maxResults);
    }

    if (isNaN(maxResults)) {
        res.status(400).json(formatErrorBody("Max results is not a valid number"));
    } else if (maxResults <= 0) {
        res.status(400).json(formatErrorBody("Max results must be a number larger than 0."));
    } else {
        req.pagination = {
            maxResults: maxResults
            // TODO: nextToken
        };
        next();
    }
};