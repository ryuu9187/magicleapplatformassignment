const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const DOWNLOAD_EXPIRY_SECS = 5 * 60; // 5min
const fs = require('fs');

const presignDownload = (bucket, key, filename) => {
    return s3.getSignedUrl('getObject', {
        Bucket: bucket,
        Key: key,
        Expires: DOWNLOAD_EXPIRY_SECS,
        ResponseContentDisposition: `attachment; filename="${filename}"`
    });
};

const putObject = async (bucket, key, file) => {
    const params = {
        Bucket: bucket,
        Key: key,
        Body: fs.createReadStream(file.path, { encoding: 'utf8'}),
        ContentType: file.type
    };

    return new Promise((resolve, reject) => {
        s3.putObject(params, (error, data) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(data);
        });
    });
};

module.exports = {
    /**
     * Temporariliy allows the download of a file via a URL
     * 
     * @param {String} bucket the S3 bucket where the file is located
     * @param {String} key the location of the file within the bucket
     * @param {String} filename the save-as name for the file download
     * @returns the file's presigned URL
     */
    presignDownload: presignDownload,

    /**
     * Puts the specified file in storage
     * 
     * @param {String} bucket the S3 bucket to put the file in
     * @param {String} key the location of the file within the bucket
     * @param {Object} file the file reference object
     */
    put: putObject
};