const UUID = require('node-uuid');
const Repository = require("./ddb-repository")(process.env.ASSET_TABLE);

const AssetRepository = {};

/**
 * Creates a new Asset
 * @param {Object} asset the Asset model to persist
 */
AssetRepository.create = async (asset) => {
    await Repository.save(asset);
    return asset;
};

/**
 * Retrieves an Asset by its unique identifier
 * @param {String} id the id of the asset
 * @returns the matching Asset
 */
AssetRepository.getById = async (id) => {
    return await Repository.getByKey({ id: id });
};

/**
 * Lists the assets for a user (not implemented)
 * @param {Object} pagination the pagination parameters for the request
 * @returns the list of assets found
 */
AssetRepository.list = async (pagination) => {
    return await Repository.list(pagination);
};

module.exports = AssetRepository;