const AWS = require('aws-sdk');
const dynamoDbClient = new AWS.DynamoDB.DocumentClient();
const { errors } = require('appstore-common');

/**
 * Creates a generic repository used for performing basic CRUD operations
 * @param {Object} dynamoDbClient the DDB client used for performing requests
 * @param {String} tableName the name of the table in DDB
 */
const Repository = (tableName) => {
    return {

        /**
         * Retrieves a single item from the DDB store, if it exists
         * @param {Object} key a map containing the required Hash and Range key values
         * @returns the matching item
         * @throws NotFoundError if the item does not exist
         */
        getByKey: async (key) => {
            const params = {
                TableName: tableName,
                Key: key
            };

            return new Promise((resolve, reject) => {
                dynamoDbClient.get(params, (error, data) => {
                    if (error ) {
                        reject(error);
                        return;
                    }

                    if (!data.Item) {
                        reject(errors.NOT_FOUND_ERROR);
                    } else {
                        resolve(data.Item);
                    }
                });
            });
        },

        /**
         * Lists items in the DDB store
         * @param {Object} pagination the pagination parameters for the request
         * @returns the list of items found
         */
        list: async (pagination) => {
            const params = {
                TableName: tableName,
                Limit: pagination.maxResults,
                ExclusiveStartKey: pagination.nextToken
            };

            return new Promise((resolve, reject) => {
                dynamoDbClient.scan(params, (error, data) => {
                    if (error) {
                        reject(error);
                        return;
                    }
                    resolve(data.Items);
                });
            });
        },

        /**
         * Saves an item into the DDB store, or updates it if it already exists
         * @param {Object} item the item to save
         */
        save: async (item) => {
            const params = {
                TableName: tableName,
                Item: item
            };

            return new Promise((resolve, reject) => {
                dynamoDbClient.put(params, (error, data) => {
                    if (error) {
                        reject(error);
                        return;
                    }
                    resolve();
                });
            });
        }
    };
};

module.exports = Repository;