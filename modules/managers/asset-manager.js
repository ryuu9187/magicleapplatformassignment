const ASSET_BUCKET = process.env.ASSET_BUCKET;
const { FileStorageAccessor } = require('appstore-accessors');
const { AssetRepository } = require('appstore-data-access');
const UUID = require('node-uuid');

const createAsset = async (file) => {
    const id = UUID.v4();
    const key = `assets/${id}`;

    // Upload
    const fileDetails = await FileStorageAccessor.put(ASSET_BUCKET, key, file);

    // Save
    const asset = {
        id: id,
        contentLocation: key,
        fileName: file.originalFilename,
        fileSize: file.size
    };
    
    return await AssetRepository.create(asset);
};

const getAssetById = async (id, generateDownloadUrl) => {
    const asset = await AssetRepository.getById(id);
    const key = asset.contentLocation;
    const name = asset.fileName;

    if (generateDownloadUrl) {
        asset.downloadUrl = FileStorageAccessor.presignDownload(ASSET_BUCKET, key, name);
    }

    return asset;
};

const listAssets = async (pagination) => {
    return await AssetRepository.list(pagination);
};

module.exports = {
    /**
     * Creates a new asset
     * 
     * @param {Object} file the file metadata of the asset
     * @returns the created Asset
     */
    create: createAsset,

    /**
     * Retrieves an asset by its unique identifier and optionally generates a download url for the content
     * 
     * @param {String} id the id of the Asset
     * @param {Boolean} generateDownloadUrl whether to generate a download url for the Asset
     * @returns the Asset, if it exists
     */
    getById: getAssetById,

    /**
     * Performs a paginated query of assets
     * 
     * @param {Object} pagination the pagination request
     * @returns a list of assets
     */
    list: listAssets
};