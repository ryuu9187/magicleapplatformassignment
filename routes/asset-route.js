const express = require('express');
const multipart = require('connect-multiparty');
const asyncErrorHandler = require('../middleware/async-error-handler');
const paginated = require('../middleware/pagination-param-parser');

const { AssetManager } = require('appstore-managers');
const transformer = require('../transformers/asset-transformer');

const getAsset = asyncErrorHandler(async (req, res) => {
    const asset = await AssetManager.getById(req.params.id);
    res.json(transformer.toModel(asset));
});

const listAssets = asyncErrorHandler(async (req, res) => {
    const assets = await AssetManager.list(req.pagination);
    res.json(assets.map(x => transformer.toModel(x)));
});

const createAsset = asyncErrorHandler(async (req, res) => {
    const asset = await AssetManager.create(req.files["content"]);
    res.status(201).json(transformer.toModel(asset));
});

module.exports = (app) => {
    const router = express.Router();
    router.get('/:id', getAsset);
    router.get('/', [paginated(25)], listAssets);
    router.post('/', [multipart()], createAsset);

    app.use('/appstore/assets', router);
};