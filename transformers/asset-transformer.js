const transformToModel = (asset) => {
    const { id, fileName, fileSize, downloadUrl } = asset;
    return { id, fileName, fileSize, downloadUrl };
};

module.exports = {
    /**
     * Transforms an Asset object to a client-presentable model
     * 
     * @param {Object} asset the Asset domain object
     * @returns the client model representation
     */
    toModel: transformToModel
};