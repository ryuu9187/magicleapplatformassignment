module.exports = {
    /**
     * Formats the body of an HTTP response containing an error
     * 
     * @param {String|Object} message the error message 
     * @returns the body of the formatted HTTP response
     */
    formatErrorBody: (message) => {
        return { message: message };
    }
};